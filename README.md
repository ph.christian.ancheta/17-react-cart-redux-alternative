# Cart Redux Alternative - React App
A simple React App that shows storing cross-component states without the use of Redux Library. Users would be able to navigate through pages wherein when favorites are set, these will be also accessable or marked to the other pages as their state changes.

## Setup

```
npm install
npm run start
```
